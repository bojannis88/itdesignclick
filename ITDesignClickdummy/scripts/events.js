﻿$(document).ready(function () {
    //switch click
    if ($(".js-switch").length > 0) {
        $(".js-switch").click(function () {
            $("body").toggleClass("cxo-enabled");
        });
    }

    if ($(".js-scrolling-card-container").length > 0) {
        $(window).scroll(function () {
            var bodyRect = document.body.getBoundingClientRect(),
            elemRect = $(".scrolling-card")[0].getBoundingClientRect(),
            elemContainerRect = $(".js-scrolling-card-container")[0].getBoundingClientRect(),
            offset = elemRect.top - bodyRect.top;

            if (elemContainerRect.bottom > $(".scrolling-card").height() + 90) {
                var topOffset = 0 - elemRect.top + parseInt($(".scrolling-card").css("top")) + 90 > 180 ? 0 - elemRect.top + parseInt($(".scrolling-card").css("top")) + 90 : 0;
            }
            $(".scrolling-card").css("top", topOffset);
        });
    }
});